# Problem Set 2, hangman.py
# Name: 
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string
import re

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    count_cur = 0
    count_num = len(secret_word)
    for i in secret_word:
        for j in letters_guessed:
            if (i == j):
                count_cur += 1

    if (count_cur == count_num):
        return True
    else:
        return False



def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    string_out =""
    flag = 0
    for i in secret_word:
        for j in letters_guessed:
            if (i == j):
                flag =1

        if (flag == 1):
            string_out += i
        else:
            string_out += "_"
        string_out+= " "
        flag = 0

    return string_out




def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    alpha = "abcdefghijklmnopqrstuvwxyz"
    #alpha.ascii_letters() пока отдохнёт
    copy = ""
    flag = 0
    for i in alpha:

        for j in letters_guessed:
            if (i == j):
                flag = 1

        if (flag == 0):
            copy+=i
        flag = 0
    return copy
    
def is_guess_true(secret_word,last_letter):
    for i in secret_word:
        if i == last_letter:
            return True
    return False

def hangman(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!
    
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
    
    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    print("Welcome to Hangman!")
    count = 6
    len_w = len(secret_word)
    print(f"I am thinking of a word that is {len_w} letters long")
    warnings = 3
    print("---------------------------------")
    inp = ""
    letters_guessed = []
    vowels = ["a", "i", "o", "e", "u"]
    while (count > 0) :
        if is_word_guessed(secret_word,letters_guessed) == True:
            break
        print(f"You have {count} guesses left")
        print("Avalibale letters:"+get_available_letters(letters_guessed))

        inp = str(input())

        if (len(inp) == 1 and inp.isalpha() == True):
            letters_guessed.append(inp.lower())
            if (is_guess_true(secret_word,letters_guessed[-1]) == True):
                print("Great guess:" + get_guessed_word(secret_word,letters_guessed))
            else:
                sw = []
                co_no_vow = 0
                for i in secret_word:
                    if sw.__contains__(i):
                        pass
                    else:
                        sw.append(i)
                for i in vowels:
                    if sw.__contains__(i):
                        break
                    co_no_vow += 1
                if co_no_vow == 5:
                    count -= 1
                print("Oops! That letter is not in my word:" + get_guessed_word(secret_word,letters_guessed))
                count -= 1

        else:
            if warnings == 1:
                count -= 1
                warnings = 3
                print("You have got a penalty. Minus 1 guess")

            else:
                warnings -= 1
                print(f"Oops! That is not a valid letter. You have {warnings}warnings left")

    if is_word_guessed(secret_word,letters_guessed) == True:
        uniq = []
        for i in secret_word:
            if uniq.__contains__(i):
                pass
            else:
                uniq.append(i)
        total_score = count * len(uniq)
        print(f"U won, your score:{total_score}")
    else:
        print(f"You lose. Right word:{secret_word}")



# When you've completed your hangman function, scroll down to the bottom
# of the file and uncomment the first two lines to test
#(hint: you might want to pick your own
# secret_word while you're doing your own testing)


# -----------------------------------



def match_with_gaps(my_word, other_word):
    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the 
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise: 
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    pattern = ""
    cur = ""
    cur = ""
    out_arr = []
    for i in my_word:
        cur = i
        if cur.isalpha() == True:
            pattern += i
        elif (cur != " "):
            pattern += "\w"
        cur = ""
    return re.match(pattern,other_word)


def show_possible_matches(my_word, secret_word):
    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.

    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    pattern = ""
    cur = ""
    out_arr = []
    for i in my_word:
        cur = i
        if cur.isalpha() == True:
            pattern += i
        elif (cur != " "):
            pattern += "\w"
        cur = ""
    for j in wordlist:
        if (len(j) == len(secret_word)):
            if re.match(pattern, j) != None:
                out_arr.append(j)
    return out_arr

def hangman_with_hints(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses
    
    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Make sure to check that the user guesses a letter
      
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
      
    * If the guess is the symbol *, print out all words in wordlist that
      matches the current guessed word. 
    
    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    print("Welcome to Hangman!")
    count = 6
    len_w = len(secret_word)
    print(f"I am thinking of a word that is {len_w} letters long")
    warnings = 3
    print("---------------------------------")
    inp = ""
    letters_guessed = []
    vowels = ["a","i","o","e","u"]
    while (count > 0):
        inp = ""
        if is_word_guessed(secret_word, letters_guessed) == True:
            break
        print(f"You have {count} guesses left")
        print("Avalibale letters:" + get_available_letters(letters_guessed))

        inp = str(input())

        if (len(inp) == 1 and inp.isalpha() == True):
            letters_guessed.append(inp.lower())
            if (is_guess_true(secret_word, letters_guessed[-1]) == True):
                print("Great guess:" + get_guessed_word(secret_word, letters_guessed))
            else:
                sw = []
                co_no_vow = 0
                for i in secret_word:
                    if sw.__contains__(i):
                        pass
                    else:
                        sw.append(i)
                for i in vowels:
                    if sw.__contains__(i):
                        break
                    co_no_vow +=1
                if co_no_vow == 5:
                    count -= 1
                print("Oops! That letter is not in my word:" + get_guessed_word(secret_word, letters_guessed))
                count -= 1

        elif inp == "*":

            arr = show_possible_matches(get_guessed_word(secret_word,letters_guessed), secret_word)
            if len(arr) != 0:
                for i in arr:
                    print(i, end=' ')
                print(".")
            else:
                print("There's no match words")
            arr = []

        else:
            if warnings == 1:
                count -= 1
                warnings = 3
                print("You have got a penalty. Minus 1 guess")

            else:
                warnings -= 1
                print(f"Oops! That is not a valid letter. You have {warnings}warnings left")

    if is_word_guessed(secret_word, letters_guessed) == True:
        uniq = []
        for i in secret_word:
            if uniq.__contains__(i):
                pass
            else:
                uniq.append(i)
        total_score = count * len(uniq)
        print(f"U won, your score:{total_score}")
    else:
        print(f"You lose. Right word:{secret_word}")



# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.


if __name__ == "__main__":
    # pass

    # To test part 2, comment out the pass line above and
    # uncomment the following two lines.
    
    #secret_word = choose_word(wordlist)
    #hangman(secret_word)

###############
    
    # To test part 3 re-comment out the above lines and 
    # uncomment the following two lines. 
    
    secret_word = choose_word(wordlist)
    #secret_word = "brrrr"
    hangman_with_hints(secret_word)
    #print(type(wordlist))